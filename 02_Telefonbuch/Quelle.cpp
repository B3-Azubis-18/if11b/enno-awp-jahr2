#include<iostream>
#include<string>

#include "lib/rapidjson/document.h"
#include "lib/rapidjson/writer.h"
#include "lib/rapidjson/stringbuffer.h"

#include"Quelle.h"

Telefonbuch telefonbuch;

void zeigeMenue()
{
	std::cout << "---------------------------------------------------------" << std::endl;
	std::cout << "------------ Mein - Telefonbuch -------------------------" << std::endl;
	std::cout << "---------------------------------------------------------" << std::endl;
	std::cout << "1. Telefonbuch anzeigen ---------------------------------" << std::endl;
	std::cout << "2. Neuen Eintrag anlegen---------------------------------" << std::endl;
	std::cout << "3. Suche Eintr�ge anhand des Namens----------------------" << std::endl;
	std::cout << "4. L�sche Eintr�ge anhand der Identifikations-Nummer-----" << std::endl;
	std::cout << "5. Beenden-----------------------------------------------" << std::endl;
	std::cout << std::endl;
	std::cout << "Ihre Auswahl:";
}

bool schreibe_eintrag()
{
	std::string name;
	std::string telefonnr;
	char bestaetigung;

	std::cout << "Bitte den Namen eingeben: ";
	std::cin >> name;
	std::cout << "Bitte Telefonnr eingeben: ";
	std::cin >> telefonnr;

	// TODO: Datenvalidierung

	std::cout << "Name: \"" << name << "\" Telefonnr: " << telefonnr << std::endl;
	std::cout << "Korrekt? Y f�r \"Ja\", alles andere f�r \"Nein\"" << std::endl;
	std::cin >> bestaetigung;

	if (bestaetigung == 'c')
	{
		telefonbuch.current_max_ID++;

		T_Telefoneintrag tmp_eintrag;
		tmp_eintrag.ID = telefonbuch.current_max_ID;
		tmp_eintrag.name = name;
		tmp_eintrag.telefonnr = telefonnr;

		telefonbuch.entrys++;
		telefonbuch.eintraege[telefonbuch.entrys] = tmp_eintrag;
		return true;
	}
	else
	{
		return false;
	}
}

void liste_anzeigen()
{
	std::cout << "##### Vollstaendiges Telefonbuch #####" << std::endl;
	for (int i = 0; i < telefonbuch.entrys; i++)
	{
		if (telefonbuch.eintraege[i] == EMPTY) { continue; }
		eintrag_anzeigen(telefonbuch.eintraege[i]);
		std::cout << std::endl;
	}
}

void eintrag_anzeigen(T_Telefoneintrag eintrag)
{
	std::cout << "### " << eintrag.name << std::endl;
	std::cout << "# Nr: " << eintrag.telefonnr << std::endl;
	std::cout << "# ID: " << eintrag.ID << std::endl;
}

bool suche_eintrag(std::string name)
{

	for (int i = 0; i < telefonbuch.entrys; i++)
	{
		if (telefonbuch.eintraege[i].name == name)
		{
			return true;
		}
	}
	return false;
}

bool loesche_eintrag(int ID)
{
	unsigned int entry_position = 0;
	bool entry_found = false;

	for (int i = 0; i < telefonbuch.entrys; i++)
	{
		if (telefonbuch.eintraege[i].ID == ID)
		{
			entry_position = i;
			entry_found = !entry_found;
		}
	}

	if (!entry_found)
	{
		return false;
	}

	if (entry_position + 1 > MAX_ENTRYS)
	{
		telefonbuch.eintraege[entry_position] = EMPTY;
		telefonbuch.entrys--;
		return true;
	}
	
	for (int i = entry_position; i < telefonbuch.entrys; i++)
	{
		if (i == MAX_ENTRYS)
		{
			telefonbuch.eintraege[i] = EMPTY;
			break;
		}
		telefonbuch.eintraege[i] = telefonbuch.eintraege[i + 1];
	}

	return true;
}

void sortiere_id()
{
	return;
}

void sortiere_name()
{
	return;
}

void telefonbuch_schreiben()
{
	return;
}

void telefonbuch_lesen()
{
	return;
}


int main()
{
	std::cout << "To be implemented!" << std::endl;

	getchar();
	getchar();

	return 0;
}
