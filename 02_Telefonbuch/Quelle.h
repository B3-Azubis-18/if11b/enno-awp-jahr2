#pragma once
#include<string>

static const unsigned int MAX_ENTRYS = 100;

struct T_Telefoneintrag {
	unsigned int ID;
	std::string name;
	std::string telefonnr;
};

struct Telefonbuch {
	unsigned int current_max_ID;
	unsigned int entrys;
	T_Telefoneintrag eintraege[MAX_ENTRYS + 1];
};

static const T_Telefoneintrag EMPTY = {
	MAX_ENTRYS + 1,
	"empty",
	"000000"
};

bool operator==(const T_Telefoneintrag &telefoneintrag1, const T_Telefoneintrag &telefoneintrag2) {
	return telefoneintrag1.ID == telefoneintrag2.ID
		&& telefoneintrag1.name == telefoneintrag2.name
		&& telefoneintrag1.telefonnr == telefoneintrag2.telefonnr;
}

bool operator==(const Telefonbuch &telefonbuch1, const Telefonbuch &telefonbuch2) {
	return telefonbuch1.eintraege == telefonbuch2.eintraege
		&& telefonbuch1.current_max_ID == telefonbuch2.current_max_ID
		&& telefonbuch1.entrys == telefonbuch2.entrys;
}

void zeigeMenue();
bool schreibe_eintrag();
void liste_anzeigen();
void eintrag_anzeigen(T_Telefoneintrag);
bool suche_eintrag(std::string);
bool loesche_eintrag(int);
void sortiere_id();
void sortiere_name();
void telefonbuch_schreiben();
void telefonbuch_lesen();
int main();
