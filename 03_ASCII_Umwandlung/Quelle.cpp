#include <iostream>

using namespace std;


void ASCII_Umwandlung(char[], int);

int main()
{

	while (true)
	{
		char acZeichen[21] = { '\0' };
		int iAnzahl = 0;

		cout << "Geben Sie bis zu 20 Zeichen ein: ";
		cin >> acZeichen;

		iAnzahl = 20;

		acZeichen[iAnzahl] = '\0'; //das 21. Zeichen bricht den Abruf in der Funktion ab.

		//�bergabe des Pointer Arrays mit L�nge
		ASCII_Umwandlung(acZeichen, iAnzahl);

		system("pause");
	}

	return 0;
}

void ASCII_Umwandlung(char p_acZeichen[], int iLaenge)
{
	cout << "Zeichen" << '\t' << "ASCII-Wert" << endl;
	//Zeilenausgabe bis entweder \0 oder die maximale Anzahl erreicht ist
	std::cout << "\"";
	for (int i = 0; i < iLaenge; i++) {
		if (p_acZeichen[i] == '\0') { break; }
		std::cout << p_acZeichen[i];
	}
	std::cout << "\"";
	std::cout << std::endl;
}


