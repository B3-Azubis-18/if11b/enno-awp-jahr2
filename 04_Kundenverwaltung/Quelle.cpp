#include<iostream>
#include<sstream>
#include<regex>
#include<string>
#include<Windows.h>

#include"Quelle.h"

int stringtoint(std::string stringnumber) {
	// object from the class stringstream 
	std::stringstream geek(stringnumber);

	// The object has the value 12345 and stream 
	// it to the integer x 
	int x = -1;
	geek >> x;

	// Now the variable x holds the value 12345 
	return x;
}

void menue()
{
	std::cout << "Nettmann" << std::endl;
	std::cout << "---------------------------------" << std::endl;
	std::cout << "Kundenverwaltung" << std::endl;
	std::cout << "---------------------------------" << std::endl;
	std::cout << u8"Bitte w�hlen Sie:" << std::endl;
	std::cout << "1. Kunde anzeigen" << std::endl;
	std::cout << u8"2. Kunde hinzuf�gen" << std::endl;
	std::cout << "3. Kunde suchen" << std::endl;
	std::cout << "4. Kunde loeschen" << std::endl;
	std::cout << "5. Beenden" << std::endl;
	std::cout << std::endl;
	std::cout << "Ihre Auswahl: ";
}

void kunde_hinzufuegen(Kunde kunde, Kundenarray *Kundenverzeichnis)
{
	// Get memory for new Customer
	Kunde *new_Kundenverzeichnis = new Kunde[Kundenverzeichnis->laenge + 1];

	// Save old customers in new Array
	for (int i = 0; i < Kundenverzeichnis->laenge; i++) {
		new_Kundenverzeichnis[i] = Kundenverzeichnis->kunden[i];
	}
	//Save new customer in new Array
	new_Kundenverzeichnis[Kundenverzeichnis->laenge] = kunde;
	Kundenverzeichnis->laenge++;

	// Delte old Array
	delete[] Kundenverzeichnis->kunden;
	// Replace pointer of old Array with new Array
	Kundenverzeichnis->kunden = new_Kundenverzeichnis;
}

void kunde_loeschen(Kunde kunde, Kundenarray *Kundenverzeichnis)
{
	int index = kunde_suchen(kunde.vorname, Kundenverzeichnis);
	if (index != -1) {
		Kunde *new_Kundenverzeichnis = new Kunde[(Kundenverzeichnis->laenge) - 1];
		for (int i = 0; i < index; i++) {
			new_Kundenverzeichnis[i] = Kundenverzeichnis->kunden[i];
		}
		for (int i = index; i < Kundenverzeichnis->laenge - 1; i++) {
			new_Kundenverzeichnis[i] = Kundenverzeichnis->kunden[i + 1];
		}
		delete[] Kundenverzeichnis->kunden;
		Kundenverzeichnis->kunden = new_Kundenverzeichnis;
		Kundenverzeichnis->laenge--;
	}
	else
	{
		std::cout << "Kunde konnte nicht gefunden werden!";
	}
}

void kunde_anzeigen(Kunde *kunde)
{
	std::cout << "Vorname: " << kunde->vorname << std::endl;
	std::cout << "Nachname: " << kunde->nachname << std::endl;
	std::cout << "Telefonnummer: " << kunde->telefonnummer << std::endl;
}

Kundenarray kunden_sortieren(Kundenarray kunden, int sort_selection)
{
	if (kunden.laenge <= 1) {
		return kunden;
	}

	Kundenarray sorted;
	sorted.laenge = 0;
	sorted.kunden = new Kunde[0];

	switch (sort_selection)
	{
	case SORT_FIRSTNAME:
	{
		//
	}
	case SORT_SURNAME:
	{
		//
	}
	case SORT_TELEPHONE:
	{
		while (kunden.laenge > 1)
		{
			Kunde temp = kunden.kunden[0];
			for (int i = 1; i < kunden.laenge; i++)
			{
				if (temp.telefonnummer > kunden.kunden[i].telefonnummer)
				{
					temp = kunden.kunden[i];
				}
			}
			kunde_hinzufuegen(temp, &sorted);
			kunde_loeschen(temp, &kunden);
		}
		kunde_hinzufuegen(kunden.kunden[0], &sorted);
		break;
	}
	default:
	{
		return kunden;
	}
	}
	return sorted;
}

int kunde_suchen(std::string tobefound, Kundenarray *Kundenverzeichnis)
{
	int index = -1;
	index = kunde_suchen_vorname(tobefound, Kundenverzeichnis);
	if (index != -1) { return index; }
	index = kunde_suchen_nachname(tobefound, Kundenverzeichnis);
	if (index != -1) { return index; }
	index = kunde_suchen_telefonnr(stringtoint(tobefound), Kundenverzeichnis);
	if (index != -1) { return index; }
	return -1;
}

int kunde_suchen_vorname(std::string vorname, Kundenarray *Kundenverzeichnis)
{
	for (int i = 0; i < Kundenverzeichnis->laenge; i++) {
		if (Kundenverzeichnis->kunden[i].vorname == vorname) {
			return i;
		}
	}
	return -1;
}

int kunde_suchen_nachname(std::string nachname, Kundenarray *Kundenverzeichnis)
{
	for (int i = 0; i < Kundenverzeichnis->laenge; i++) {
		if (Kundenverzeichnis->kunden[i].nachname == nachname) {
			return i;
		}
	}
	return -1;
}

int kunde_suchen_telefonnr(int telefonnr, Kundenarray *Kundenverzeichnis)
{
	for (int i = 0; i < Kundenverzeichnis->laenge; i++) {
		if (Kundenverzeichnis->kunden[i].telefonnummer == telefonnr) {
			return i;
		}
	}
	return -1;
}

void kunden_anzeigen(Kundenarray *kundenverzeichnis)
{
	std::cout << "##### KUNDENVERZEICHNIS #####" << std::endl;
	for (int i = 0; i < kundenverzeichnis->laenge; i++) {
		kunde_anzeigen(&(kundenverzeichnis->kunden[i]));
		std::cout << std::endl;
	}
	std::cout << "------------------------------------" << std::endl;
}

bool validate_telephone_number(std::string number) {
	std::regex regex_telephone_number("((\+\d{1,3})|([0]{2}\d{2,4}))?([ ])?(\(?\d{3,6}\)?)?\/?([ ])?\d{3,}$");

	return regex_match(number, regex_telephone_number);
}

int main()
{
	SetConsoleOutputCP(CP_UTF8);

	unsigned int eingabe = 0;
	Kundenarray *Kundenverzeichnis = new Kundenarray;
	Kunde maxmustermann = { "Max", "Mustermann", 123456 };
	Kundenverzeichnis->kunden = new Kunde[1];
	Kundenverzeichnis->kunden[0] = maxmustermann;
	Kundenverzeichnis->laenge = 1;

	while (eingabe != 5)
	{
		menue();

		std::cin >> eingabe;

		switch (eingabe)
		{
		case 1:
		{
			kunden_anzeigen(&kunden_sortieren(*Kundenverzeichnis, SORT_TELEPHONE));

			getchar();
			getchar();
			break;
		}
		case 2:
		{
			std::string vorname;
			std::string nachname;
			int telefonnr;
			std::cout << "Vorname: ";
			std::cin >> vorname;
			std::cout << "Nachname: ";
			std::cin >> nachname;
			std::cout << "Telefonnr: ";
			std::cin >> telefonnr;
			Kunde kunde = { vorname, nachname, telefonnr };

			kunde_hinzufuegen(kunde, Kundenverzeichnis);
			break;
		}
		case 3:
		{
			std::string tobefound;
			std::cout << "Geben sie den Vor- oder Nachnamen bzw die Telefonnr ein: ";
			std::cin >> tobefound;
			int index = kunde_suchen(tobefound, Kundenverzeichnis);
			if (index != -1) {
				kunde_anzeigen(&(Kundenverzeichnis->kunden[index]));
			}
			else
			{
				std::cout << "Not found!" << std::endl;
			}
			std::cout << u8"Enter zum Fortfahren bitte dr�cken!" << std::endl;
			getchar();
			getchar();
			break;
		}
		case 4:
		{
			std::string tobefound;
			std::cout << "Geben sie den Vor- oder Nachnamen bzw die Telefonnr ein: ";
			std::cin >> tobefound;
			int index = kunde_suchen(tobefound, Kundenverzeichnis);
			if (index != -1) {
				kunde_anzeigen(&(Kundenverzeichnis->kunden[index]));
				std::cout << u8"Wird nun gel�scht!" << std::endl;
				kunde_loeschen(Kundenverzeichnis->kunden[index], Kundenverzeichnis);
				std::cout << u8"Ist gel�scht worden!" << std::endl;
			}
			else
			{
				std::cout << u8"Nicht gefunden! Es wird nichts gel�scht!" << std::endl;
			}
			std::cout << u8"Enter zum Fortfahren bitte dr�cken!" << std::endl;
			getchar();
			getchar();
			break;
		}
		case 5:
		{
			std::cout << u8"Auf wiedersehen ihre Kunden werden gel�scht!" << std::endl;
			break;
		}
		default:
			std::cout << "You piece of **** give me a number between 1 and 5!" << std::endl;
			break;
		}
		system("cls");
	}

	return 0;
}