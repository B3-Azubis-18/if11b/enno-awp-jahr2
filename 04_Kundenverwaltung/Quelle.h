#pragma once

struct Kunde
{
	std::string vorname;
	std::string nachname;
	int telefonnummer;
};

struct Kundenarray {
	int laenge;
	Kunde *kunden;
};

static const int SORT_FIRSTNAME = 0;
static const int SORT_SURNAME = 1;
static const int SORT_TELEPHONE = 2;

void menue();
void kunde_hinzufuegen(Kunde, Kundenarray*);
void kunde_loeschen(Kunde, Kundenarray*);
void kunde_anzeigen(Kunde*);
Kundenarray kunden_sortieren(Kundenarray, int);
int kunde_suchen(std::string, Kundenarray*);
int kunde_suchen_vorname(std::string, Kundenarray*);
int kunde_suchen_nachname(std::string, Kundenarray*);
int kunde_suchen_telefonnr(int, Kundenarray*);
void kunden_anzeigen(Kundenarray*);
bool validate_telephone_number(std::string);

int main();