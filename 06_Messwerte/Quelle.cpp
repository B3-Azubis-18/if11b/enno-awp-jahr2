#include <iostream>

using namespace std;

// Prototypen
void addNewValue(int * &piArray, int &iLaenge, int iEintrag);
double calcAverage(int *piArray, int iLaenge);
bool einlesen(FILE *fpDatei, int*& piArray, int &iLaenge);
bool schreiben(FILE *fpDatei, int *piArray, int iLaenge);

bool einlesen(FILE *fpDatei, int*& piArray, int &iLaenge) {
	int tempMesswert;
	fopen_s(&fpDatei, "Messwerte.datei","r");
	if (fpDatei == nullptr) {
		return false;
	}
	else
	{
		do {
			fread(&tempMesswert, sizeof(int), 1, fpDatei);
			if (!feof(fpDatei)) {
				addNewValue(piArray, iLaenge, tempMesswert);
			}
		} while (!feof(fpDatei));
		fclose(fpDatei);
		return true;
	}
}

bool schreiben(FILE *fpDatei, int *piArray, int iLaenge) {
	fopen_s(&fpDatei, "Messwerte.datei", "w");

	if (fpDatei == nullptr) {
		return true;
	}
	else
	{
		iLaenge = fwrite(piArray, sizeof(int), iLaenge, fpDatei);
		fclose(fpDatei);
		return false;
	}
}


int main() {

	int iAnzahlMessungen = 0;
	int iNeuerMesswert = 0;
	char cEingabe = ' ';
	int * piMeineMessungen = nullptr;

	do {
		cout << "(a) Messungen anzeigen " << endl;
		cout << "(e) Messung eintragen" << endl;
		cout << "(l) Bildschirm leeren" << endl;
		cout << "(r) Messungen eintragen" << endl;
		cout << "(w) Messungen lesen" << endl;
		cout << "Auswahl: ";
		cin >> cEingabe;
		cout << endl;

		cin.clear();

		switch (cEingabe) {

		case 'a':
		case 'A':
			if (iAnzahlMessungen > 0) {
				for (int i = 0; i < iAnzahlMessungen; i++) {
					cout << "Wert " << i + 1 << ":\t" << piMeineMessungen[i] << endl;
				}

				cout << "calcAverage: " << calcAverage(piMeineMessungen, iAnzahlMessungen) << endl;

			}
			else {
				cout << "### Keine Messwerte vorhanden! ###" << endl << endl;
			}
			break;

		case 'e':
		case 'E':

			cEingabe = 'j';

			while (cEingabe == 'j' || cEingabe == 'J') {
				cout << "Messwert: ";
				cin >> iNeuerMesswert;
				addNewValue(piMeineMessungen, iAnzahlMessungen, iNeuerMesswert);

				cout << "Moechten Sie ein Wert einf�gen (j): ";
				cin >> cEingabe;
			}
			break;

		case 'l':
		case 'L':
			system("CLS");
			break;
		case 'r':
		case 'R':
			// TODO
			break;
		case 'w':
		case 'W':
			// TODO
			break;
		default:
			cout << "### Eingabe ungueltig! ###" << endl << endl;
			break;
		}

		cin.clear();
		cin.ignore(1, '\0');
	} while (cEingabe != 'n' || cEingabe != 'N');

	return 0;
}

// Neuen Messwert hinzuf�gen
void addNewValue(int * &piArray, int &iLaenge, int iEintrag) {

	int * piTempArray = piArray;

	piArray = new int[iLaenge + 1];
	piArray[iLaenge] = iEintrag;

	for (int i = 0; i < iLaenge; i++) {
		piArray[i] = piTempArray[i];
	}

	delete[] piTempArray;
	iLaenge++;
}


// Berechne den Durchschnitt und gebe diesen zur�ck
double calcAverage(int *piArray, int iLaenge) {

	double sum = 0.0;
	double average = 0.0;

	for (int i = 0; i < iLaenge; i++) {
		sum += piArray[i];
	}

	average = sum / iLaenge;

	return average;
}