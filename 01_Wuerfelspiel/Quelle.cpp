#include <iostream>

const int iAnSpiel = 5;
struct T_Wuerfeln
{
	char cName[20];
	int iWuerfel[10];
	int iWieOft[7] = { 0 };
};
int main();

int main()
{
	T_Wuerfeln Spiel1[iAnSpiel];

	for (int i = 0; i < iAnSpiel; i++)
	{
		std::cin >> Spiel1[i].cName;
		for (int y = 0; y < 10; y++)
		{
			Spiel1[i].iWuerfel[y] = rand() % 6 + 1;
			Spiel1[i].iWieOft[Spiel1[i].iWuerfel[y]]++;
		}
	}

	for (int i = 0; i < iAnSpiel; i++)
	{
		std::cout << Spiel1[i].cName << " ";
		for (int y = 0; y < 10; y++)
		{
			std::cout << Spiel1[i].iWuerfel[y] << " ";
		}
		std::cout << std::endl;
		for (int y = 0; y < 7; y++)
		{
			std::cout << Spiel1[i].iWieOft[y] << " ";
		}
		std::cout << std::endl;
	}

	getchar();
	getchar();

	return 0;
}