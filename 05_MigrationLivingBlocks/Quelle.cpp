#include <iostream>
#include <string>

using namespace std;

struct T_Gebaeude
{
	int iGebaeudeID;
	char acStadt[25]; // nicht den Datentyp �string� verwenden!
	int iAnzahlWohnungen;
	float fVermoegenswert;
};


//Prototypen
//TODO: Zu implementierende Methoden: ExportiereGebaeude, ImportiereGebaeude, GebaeudeuebersichtAusgeben
void ExportiereGebaeude(T_Gebaeude* alleGebaeude, int iAnzahl, string sFileName);
void ImportiereGebaeude(T_Gebaeude*& livingBlocksGebaeude, int& iAnzahl, string sFileName);
void GebaeudeuebersichtAusgeben(T_Gebaeude*& alleGebaeude, int& iAnzahl, string sFileName);

T_Gebaeude EingabeGebaeude(int iAnzahl);
void AusgabeGebaeude(T_Gebaeude* alleGebaeude, int iAnzahl);
void ZeigeMenue();
string InfoZuGebaeudeErstellen(T_Gebaeude& gebaeude);


int main()
{
	locale::global(locale("German"));

	int iMaxAnzahl = 100;
	int iAnzahl = 0;
	char cEingabe = ' ';
	int iEingabe;

	string sFileName = "Testdaten\\Migrationsdatei.livingBlocks";
	string sFileNameUebersicht = "Uebersicht.greenhouseEstates";

	T_Gebaeude* alleGebaeude = new T_Gebaeude[iMaxAnzahl];

	do
	{
		ZeigeMenue();
		cin >> iEingabe;
		switch (iEingabe)
		{
		case 1: //Ausgabe der Gebaeude
			if (iAnzahl == 0)
			{
				cout << "Kein Geb�ude vorhanden!" << endl;
			}
			else
			{
				AusgabeGebaeude(alleGebaeude, iAnzahl);
			}
			break;
		case 2: //Datei lesen
			ImportiereGebaeude(alleGebaeude, iAnzahl, sFileName);
			break;
		case 3: //Gebaeude hinzuf�gen
			while (iAnzahl < iMaxAnzahl)
			{
				do
				{
					cEingabe = ' ';
					alleGebaeude[iAnzahl] = EingabeGebaeude(iAnzahl);
					iAnzahl++;
					cout << "M�chten Sie ein Geb�ude hinzuf�gen? (j/...): ";
					cin >> cEingabe;

				} while (cEingabe == 'j');
				break;
			}
			break;
		case 4: //Datei schreiben
			ExportiereGebaeude(alleGebaeude, iAnzahl, sFileName);
			break;
		case 5: //Gebaeudeuebersicht ausgeben
			GebaeudeuebersichtAusgeben(alleGebaeude, iAnzahl, sFileNameUebersicht);
			break;
		case 6: //Programm beenden
			cout << "Auf Wiedersehen" << endl;
			break;
		default:
			cout << "Eingabe ung�ltig! \a";
			break;
		}
		system("pause");
	} while (iEingabe != 6);


	delete[] alleGebaeude;
	alleGebaeude = nullptr;
	return 0;
}

//Gibt das Men� aus.
void ZeigeMenue()
{
	system("CLS");
	cout << "                                                                     #######                                         " << endl
		<< "####   #####  ###### ###### #    # #    #  ####  #    #  ####  ###### #        ####  #####   ##   ##### ######  ####  " << endl
		<< "#    # #    # #      #      ##   # #    # #    # #    # #      #      #       #        #    #  #    #   #      #      " << endl
		<< "#      #    # #####  #####  # #  # ###### #    # #    #  ####  #####  #####    ####    #   #    #   #   #####   ####  " << endl
		<< "#  ### #####  #      #      #  # # #    # #    # #    #      # #      #            #   #   ######   #   #           # " << endl
		<< "#    # #   #  #      #      #   ## #    # #    # #    # #    # #      #       #    #   #   #    #   #   #      #    # " << endl
		<< "####   #    # ###### ###### #    # #    #  ####   ####   ####  ###### #######  ####    #   #    #   #   ######  ####  " << endl;

	cout << "<1> Geb�ude anzeigen: " << endl;
	cout << "<2> Geb�ude importieren: " << endl;
	cout << "<3> Geb�ude hinzuf�gen: " << endl;
	cout << "<4> Geb�ude exportieren: " << endl;
	cout << "<5> Geb�ude�bersicht ausgeben: " << endl;
	cout << "<6> Programm beenden: " << endl;
	cout << "Auswahl: ";
}

//Erm�glicht die Eingabe eines neuen Gebaeudes
T_Gebaeude EingabeGebaeude(int iAnzahl)
{
	T_Gebaeude Gebaeude;

	Gebaeude.iGebaeudeID = iAnzahl + 1; //sollte zuk�nftig einmalig sein.
	cout << "\tGeb�ude-Nr: " << Gebaeude.iGebaeudeID << endl;
	cout << "\tStadt: ";
	cin >> Gebaeude.acStadt;
	cin.clear(); std::cin.ignore(INT_MAX, '\n');
	cout << "\tAnzahl Wohnungen: ";
	cin >> Gebaeude.iAnzahlWohnungen;
	cin.clear(); std::cin.ignore(INT_MAX, '\n');
	cout << "\tVerm�genswert: ";
	cin >> Gebaeude.fVermoegenswert;
	cin.clear(); std::cin.ignore(INT_MAX, '\n');
	cout << endl;
	return Gebaeude;
}

//Gibt die Gebaeude aus.
void AusgabeGebaeude(T_Gebaeude* alleGebaeude, int iAnzahl)
{
	for (int i = 0; i < iAnzahl; i++)
	{
		cout << InfoZuGebaeudeErstellen(alleGebaeude[i]);

		if (i == iAnzahl - 1)
		{
			cout << endl;
		}
	}
}

string InfoZuGebaeudeErstellen(T_Gebaeude& gebaeude)
{
	string info = string("Geb�udeID: ").append(to_string(gebaeude.iGebaeudeID))
		.append(", Stadt: ").append(gebaeude.acStadt)
		.append(", Anzahl Wohnungen: ").append(to_string(gebaeude.iAnzahlWohnungen))
		.append(", Verm�genswert: ").append(to_string(gebaeude.fVermoegenswert)).append(" Euro\n");

	return info;
}

//Gibt eine Uebersicht ueber alle gespeicherten Gebaeude in einer Datei aus
void GebaeudeuebersichtAusgeben(T_Gebaeude*& alleGebaeude, int& iAnzahl, string sFileName)
{
	int error_ID = 0;
	FILE* fpDatei = nullptr;
	string sAusgabe = "\0";

	cout << "Erstelle Geb�ude�bersicht..." << endl;

	//TODO: Code ergaenzen.

	cout << endl;
	cout << "##################################" << endl;
	cout << "Anzahl ausgegebener Datens�tze: " << iAnzahl << endl;
	cout << "##################################" << endl << endl;
}

//Schreibt die Gebaeude in eine Datei
void ExportiereGebaeude(T_Gebaeude* alleGebaeude, int iAnzahl, string sFileName)
{
	int error_ID = 0;
	FILE* fpDatei = nullptr;

	//TODO: Code ergaenzen.

	cout << endl;
	cout << "##################################" << endl;
	cout << "Anzahl exportierter Datens�tze: " << iAnzahl << endl;
	cout << "##################################" << endl << endl;
}

//Liest die Gebaeude aus der Datei und gibt diese aus.
void ImportiereGebaeude(T_Gebaeude*& alleGebaeude, int& iAnzahl, string sFileName)
{
	FILE* fpDatei = nullptr;
	T_Gebaeude tempGebaeude;

	fopen_s(&fpDatei, sFileName.c_str(), "r");

	if (fpDatei == nullptr) {
		std::cout << "Fehler: Datei konnte nicht ge�ffnet werden!";
	}
	else
	{
		do
		{
			fread(&tempGebaeude, sizeof(T_Gebaeude), 1, fpDatei);
			if (!feof(fpDatei)) {
				tempGebaeude.iGebaeudeID = iAnzahl + 1;
				alleGebaeude[iAnzahl] = tempGebaeude;
				iAnzahl++;
			}
		} while (!feof(fpDatei));
		
		fclose(fpDatei);
	}
}